//находим сумму рекурсией
function sumTo(n){
  if(n > 0){
    return n + sumTo(n-1);
  } else{
    return n;
  }
}

console.log(sumTo(+prompt('Введите число')));

//находим сумму через цыкл
function sumTo(n){
  var result = n
  for (i = 1; i < n; i++){
    result += i;
  }
  return result;
}

console.log(sumTo(+prompt('Введите число')));