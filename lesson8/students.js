var studentsAndPoints = ['Алексей Петров', 0, 
                        'Ирина Овчинникова', 60, 
                        'Глеб Стукалов', 30, 
                        'Антон Павлович', 30, 
                        'Виктория Заровская', 30, 
                        'Алексей Левенец', 70, 
                        'Тимур Вамуш', 30, 
                        'Евгений Прочан', 60, 
                        'Александр Малов', 0];

//задание №1
//создаем массив, в котором будут объекты с именами и баллами
var students = [];

//функция по добавлению студентов, баллов и show в каждый объект массива
function addStudentsAndPoints(name, point){
    this.name = name;
    this.point = point;
    this.show = function(){
        console.log('Студент ' + this.name + ' набрал ' + this.point + ' баллов');
    };
}

//перебираем начальный массив и добавляем данный в новые объекты нового массива
for (i = 0; i < studentsAndPoints.length; i += 2){
    students.push(new addStudentsAndPoints(studentsAndPoints[i], studentsAndPoints[i+1]));
};

//выводим список студентов
console.log('Список студентов:')
students.forEach(function(show, i){
    students[i].show();
});

//хадание №2
students.push(new addStudentsAndPoints('Николай Фролов', 0));
students.push(new addStudentsAndPoints('Олег Боровой', 0));

//задание №3

var increasePoint = function (studentName, pointPlus){
    var index = students.map(function (student) { return student.name; }).indexOf(studentName);
    return students[index].point += pointPlus;
}

increasePoint.apply(students, ['Ирина Овчинникова', 30]);
increasePoint.apply(students, ['Александр Малов', 30]);
increasePoint.apply(students, ['Николай Фролов', 10]);

//задание №4
//вывели список студентов у которых количество баллов 30+
console.log('Список студентов:');
students.forEach(function(point, i){
    if(students[i].point >= 30){
        console.log('Студент %s набрал %d баллов', students[i].name, students[i].point);
    }
});

//задание №5
function countWorks(points){
    return points/10;
}

for (i = 0; i < students.length; i++){
    students[i].worksAmount = countWorks(students[i].point);
};

//дополнительное задание
students.findByName = function (nameKey){
    var nameLength = nameKey.length;
    for (var i=0; i < this.length; i++) {
        if (this[i].name.slice(0, nameLength) === nameKey) {
            return this[i];
        }
    }
}

var resultOfSearch = students.findByName('Ирина');
console.log(resultOfSearch);
resultOfSearch = students.findByName("Люся");
console.log(resultOfSearch);
