var studentsAndPoints = ['Алексей Петров', 0, 
                        'Ирина Овчинникова', 60, 
                        'Глеб Стукалов', 30, 
                        'Антон Павлович', 30, 
                        'Виктория Заровская', 30, 
                        'Алексей Левенец', 70, 
                        'Тимур Вамуш', 30, 
                        'Евгений Прочан', 60, 
                        'Александр Малов', 0];

var max, maxIndex;

//задание №1
//создаем массив студентов
var students = studentsAndPoints.filter ( function (name){
    return typeof name == 'string';
});

//создаем массив баллов
var points = studentsAndPoints.filter ( function (point){
    return typeof point == 'number';
});

console.log(students);
console.log(points);

//задание №2
console.log('Список студентов:');
students.forEach(function(name, i, students){
  console.log('Студент ' + name + ' набрал ' + points[i] + ' баллов');
});

//задание №3
//ищем максимальное количество баллов и индекс по этому значению
points.forEach(function (point, i) {
    if (!max || point > max){
        max = point;
        maxIndex = i;
    }
})
console.log('Студент набравший максимальный балл: ' + students[maxIndex] + ' (' + max + ' баллов)');

//задание №4
function increasePoint (studentName, pointPlus){
    var index = students.map(function () { return students.indexOf(studentName) });
    return points[index[0]] += pointPlus;
}

increasePoint('Ирина Овчинникова', 30);
increasePoint('Александр Малов', 30);
console.log(points);

//Допольнительно задание
function getTop(topNumber) {
    var topIndex = [];
    if(points.length > topNumber){
      for (var i = 0; i < points.length; i++) {
          topIndex.push(i);
          if (topIndex.length > topNumber) {
              topIndex.sort(function(a, b) { return points[b] - points[a]; });
              topIndex.pop();
          }
      }
    } else{
    	for (var i = 0; i < points.length; i++) {
          topIndex.push(i);
      }
      topIndex.sort(function(a, b) { return points[b] - points[a]; });
    }
  
    var topStudentsAndPoints = [];
    for(var i = 0; i < topIndex.length; i++){
      topStudentsAndPoints.push(students[topIndex[i]], points[topIndex[i]]);
    }
  return topStudentsAndPoints;
}

function showTop(topNumber){
    console.log('Топ ' + topNumber + ':');
  for (var i = 0; i < topStudents.length; i += 2){
    console.log(topStudents[i] + ' - ' + topStudents[i+1] + ' баллов');
  }
}
var topN = 3;
var topStudents = getTop(topN);
showTop(topN);
topN = 5;
var topStudents = getTop(topN);
showTop(topN);
