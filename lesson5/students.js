var studentsAndPoints = ['Алексей Петров', 0, 
                        'Ирина Овчинникова', 60, 
                        'Глеб Стукалов', 30, 
                        'Антон Павлович', 30, 
                        'Виктория Заровская', 30, 
                        'Алексей Левенец', 70, 
                        'Тимур Вамуш', 30, 
                        'Евгений Прочан', 60, 
                        'Александр Малов', 0];
//при max=studentsAndPoints[0] - максимальное значение имя
var max=studentsAndPoints[1], maxIndex, delIndex, delStudent; 

//первое задание
console.log('Список студентов:');
for(var i = 0, imax = studentsAndPoints.length; i < imax; i+=2 ){
    console.log('Студент '+ studentsAndPoints[i] + ' набрал ' + studentsAndPoints[i+1] + ' баллов');
}

//второе задание
console.log('Студент набравший максимальный балл:');
for(var i = 1, imax = studentsAndPoints.length; i < imax; i+=2 ){
    if (studentsAndPoints[i] > max){
        max = studentsAndPoints[i];
        maxIndex = i;
    }
}
console.log('Студент '+ studentsAndPoints[maxIndex-1] + ' имеет максимальный бал ' + max);

//третье задание
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);
console.log(studentsAndPoints);

//четвертое задание
var student1 = studentsAndPoints.indexOf('Антон Павлович');
var student2 = studentsAndPoints.indexOf('Николай Фролов');
var increase = 10;

if(student1 !== -1){
studentsAndPoints[student1+1] +=10;
}

if(student2 !== -1){
studentsAndPoints[student2+1] +=10;
}

console.log(studentsAndPoints);

//пятое задание
console.log('Студенты не набравшие баллов:');
for(var i = 1, imax = studentsAndPoints.length; i < imax; i+=2 ){
    if (studentsAndPoints[i] === 0){
        console.log(studentsAndPoints[i-1]);
    }
}

//дополнительно задание
/*//удаляем студентов, у кого бал = 0
for(var i = 1, imax = studentsAndPoints.length; i < imax; i+=2 ){
    if (studentsAndPoints[i] === 0){
        delIndex = i;
        delete studentsAndPoints[delIndex];
        delete studentsAndPoints[delIndex-1];
    }
}

// studentsAndPointsGood - студенты, у кого бал более 0, убираем undefined
var studentsAndPointsGood = studentsAndPoints.filter(function(delitem){
  return delitem !== undefined;
});
console.log(studentsAndPointsGood);*/
for(var i = studentsAndPoints.length-1; i > 0; i-=2 ){
    if (studentsAndPoints[i] === 0){
        delIndex = i;
        delStudent = studentsAndPoints.splice (delIndex-1, 2);
    }
}
console.log(studentsAndPoints);