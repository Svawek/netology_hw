var studentsAndPoints = ['Алексей Петров', 0, 
                         'Ирина Овчинникова', 60, 
                         'Глеб Стукалов', 30, 
                         'Антон Павлович', 30, 
                         'Виктория Заровская', 30, 
                         'Алексей Левенец', 70, 
                         'Тимур Вамуш', 30, 
                         'Евгений Прочан', 60, 
                         'Александр Малов', 0];

function Student(name, point){
    this.name = name;
    this.point = point;
}
Student.prototype.show = function(){
    console.log('Студент ' + this.name + ' набрал ' + this.point + ' баллов');
};

var students = [];
students = new Student('Мария Яковец', 870);
students.show;